package ru.vkandyba.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @Nullable
    @Override
    public String description() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            @Nullable final String argument = command.arg();
            if (argument != null && argument.isEmpty())
                System.out.println(argument + ": " + command.description());
        }
    }

}
