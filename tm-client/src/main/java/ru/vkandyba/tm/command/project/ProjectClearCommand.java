package ru.vkandyba.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.SessionDTO;
import ru.vkandyba.tm.enumerated.Role;

public class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects...";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSession();
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getProjectEndpoint().clearProjects(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
