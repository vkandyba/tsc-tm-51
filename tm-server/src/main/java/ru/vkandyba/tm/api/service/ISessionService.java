package ru.vkandyba.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.dto.SessionDTO;

public interface ISessionService extends IService<SessionDTO> {

    @Nullable
    SessionDTO open(String login, String password);

    void close(SessionDTO sessionDTO);

    boolean checkDataAccess(String login, String password);

    void validate(SessionDTO sessionDTO);

    void validate(SessionDTO sessionDTO, Role role);

    @Nullable
    SessionDTO sign(SessionDTO sessionDTO);

}
