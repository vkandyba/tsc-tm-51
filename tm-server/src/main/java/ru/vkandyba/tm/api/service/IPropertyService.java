package ru.vkandyba.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getVersion();

    @NotNull
    String getHashSecret();

    @NotNull
    String getHashIteration();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getSignSecret();

    @NotNull
    String getSignIteration();

    @NotNull
    String getJdbcUser();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getHibernateBM2DDLAuto();

    @NotNull
    String getHibernateDialect();

    @NotNull
    String getHibernateShowSql();

    @NotNull
    String getUseSecondLvlCache();

    @NotNull
    String getCacheProviderCfg();

    @NotNull
    String getCacheRegionFactory();

    @NotNull
    String getUseQueryCache();

    @NotNull
    String getUseMinimalPuts();

    @NotNull
    String getCacheRegionPrefix();

    @NotNull
    String getUseLiteMember();

}
