package ru.vkandyba.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final String FILE_NAME = "config.properties";

    @NotNull
    private final String APPLICATION_VERSION = "application.version";

    @NotNull
    private final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private final String DEVELOPER_NAME = "developer.name";

    @NotNull
    private final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private final String DEVELOPER_EMAIL = "developer.email";

    @NotNull
    private final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private final String HASH_SECRET = "hash.secret";

    @NotNull
    private final String HASH_SECRET_DEFAULT = "";

    @NotNull
    private final String HASH_ITERATION = "hash.iteration";

    @NotNull
    private final String HASH_ITERATION_DEFAULT = "1";

    @NotNull
    private final String SERVER_HOST = "SERVER_HOST";

    @NotNull
    private final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private final String SERVER_PORT = "server.port";

    @NotNull
    private final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private final String SIGN_SECRET = "sign.secret";

    @NotNull
    private final String SIGN_SECRET_DEFAULT = "";

    @NotNull
    private final String SIGN_ITERATION = "sign.iteration";

    @NotNull
    private final String SIGN_ITERATION_DEFAULT = "1";

    @NotNull
    private final String JDBC_USER = "JDBC_USER";

    @NotNull
    private final String JDBC_USER_DEFAULT = "postgres";

    @NotNull
    private final String JDBC_PASSWORD = "JDBC_PASSWORD";

    @NotNull
    private final String JDBC_PASSWORD_DEFAULT = "postgres";

    @NotNull
    private final String JDBC_URL = "JDBC_URL";

    @NotNull
    private final String JDBC_URL_DEFAULT = "jdbc:postgresql://localhost/task-manager";

    @NotNull
    private final String JDBC_DRIVER = "JDBC_DRIVER";

    @NotNull
    private final String JDBC_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private final String HIBERNATE_DIALECT = "hibernate.dialect";

    @NotNull
    private final String HIBERNATE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private final String HIBERNATE_SHOW_SQL = "hibernate.showsql";

    @NotNull
    private final String HIBERNATE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private final String HIBERNATE_AUTO = "hibernate.hbm2dllauto";

    @NotNull
    private final String HIBERNATE_AUTO_DEFAULT = "update";

    @NotNull
    private static final String CACHE_PROVIDER_CONFIG = "hazelcast.config";

    @NotNull
    private static final String CACHE_PROVIDER_CONFIG_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String CACHE_REGION_FACTORY = "hazelcast.factory";

    @NotNull
    private static final String CACHE_REGION_FACTORY_DEFAULT
            = "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";

    @NotNull
    private static final String CACHE_REGION_PREFIX = "hazelcast.prefix";

    @NotNull
    private static final String CACHE_REGION_PREFIX_DEFAULT = "task-manager";

    @NotNull
    private static final String USE_LITE_MEMBER_VALUE = "hazelcast.lite-member";

    @NotNull
    private static final String USE_LITE_MEMBER_VALUE_DEFAULT = "true";

    @NotNull
    private static final String USE_MINIMAL_PUTS = "hazelcast.minimal-puts";

    @NotNull
    private static final String USE_MINIMAL_PUTS_DEFAULT = "true";

    @NotNull
    private static final String USE_QUERY_CACHE = "hazelcast.query";

    @NotNull
    private static final String USE_QUERY_CACHE_DEFAULT = "true";

    @NotNull
    private static final String USE_SECOND_LEVEL_CACHE = "hazelcast.level";

    @NotNull
    private static final String USE_SECOND_LEVEL_CACHE_DEFAULT = "true";



    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService(){
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if(inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getValue(@NotNull final String name, @NotNull final String defaultName){
        @Nullable final String envProperty = System.getenv(name);
        if(envProperty != null) return envProperty;
        @Nullable final String systemProperty = System.getProperty(name);
        if(systemProperty != null) return systemProperty;
        return properties.getProperty(name, defaultName);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return Manifests.read("developer");
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return Manifests.read("email");
    }

    @NotNull
    @Override
    public String getVersion() {
        return Manifests.read("build");
    }

    @NotNull
    @Override
    public String getHashSecret() {
        return getValue(HASH_SECRET, HASH_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getHashIteration() {
        return getValue(HASH_ITERATION, HASH_ITERATION_DEFAULT);
    }

    @Override
    public @NotNull String getServerHost() {
        return getValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getServerPort() {
        return getValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @Override
    public @NotNull String getSignSecret() {
        return getValue(SIGN_SECRET, SIGN_SECRET_DEFAULT);
    }

    @Override
    public @NotNull String getSignIteration() {
        return getValue(SIGN_ITERATION, SIGN_ITERATION_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcUser() {
        return getValue(JDBC_USER, JDBC_USER_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcPassword() {
        return getValue(JDBC_PASSWORD, JDBC_PASSWORD_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcUrl() {
        return getValue(JDBC_URL, JDBC_URL_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcDriver() {
        return getValue(JDBC_DRIVER, JDBC_DRIVER_DEFAULT);
    }

    @Override
    public @NotNull String getHibernateBM2DDLAuto() {
        return getValue(HIBERNATE_AUTO, HIBERNATE_AUTO_DEFAULT);
    }

    @Override
    public @NotNull String getHibernateDialect() {
        return getValue(HIBERNATE_DIALECT, HIBERNATE_DIALECT_DEFAULT);
    }

    @Override
    public @NotNull String getUseSecondLvlCache() {
        return getValue(USE_SECOND_LEVEL_CACHE, USE_SECOND_LEVEL_CACHE_DEFAULT);
    }

    @Override
    public @NotNull String getCacheProviderCfg() {
        return getValue(CACHE_PROVIDER_CONFIG, CACHE_PROVIDER_CONFIG_DEFAULT);
    }

    @Override
    public @NotNull String getCacheRegionFactory() {
        return getValue(CACHE_REGION_FACTORY, CACHE_REGION_FACTORY_DEFAULT);
    }

    @Override
    public @NotNull String getUseQueryCache() {
        return getValue(USE_QUERY_CACHE, USE_QUERY_CACHE_DEFAULT);
    }

    @Override
    public @NotNull String getUseMinimalPuts() {
        return getValue(USE_MINIMAL_PUTS, USE_MINIMAL_PUTS_DEFAULT);
    }

    @Override
    public @NotNull String getCacheRegionPrefix() {
        return getValue(CACHE_REGION_PREFIX, CACHE_REGION_PREFIX_DEFAULT);
    }

    @Override
    public @NotNull String getUseLiteMember() {
        return getValue(USE_LITE_MEMBER_VALUE, USE_LITE_MEMBER_VALUE_DEFAULT);
    }

    @Override
    public @NotNull String getHibernateShowSql() {
        return getValue(HIBERNATE_SHOW_SQL, HIBERNATE_SHOW_SQL_DEFAULT);
    }
}
