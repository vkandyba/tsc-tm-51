package ru.vkandyba.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDtoRepository extends AbstractDtoRepository<UserDTO>{

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public UserDTO findById(@NotNull String id){
        List<UserDTO> list = entityManager
                .createQuery("SELECT e FROM UserDTO e WHERE e.id = :id", UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public UserDTO findByLogin(@NotNull String login){
        List<UserDTO> list = entityManager
                .createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public void removeByLogin(@NotNull String login){
        UserDTO userDTO = findByLogin(login);
        entityManager.remove(userDTO);
    }

    public void updateUser(@NotNull String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName){
        UserDTO userDTO = findById(userId);
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setMiddleName(middleName);
        entityManager.merge(userDTO);
    }

    public void lockUserByLogin(@NotNull String login){
        UserDTO userDTO = findByLogin(login);
        userDTO.setLocked(true);
        entityManager.merge(userDTO);
    }

    public void unlockUserByLogin(@NotNull String login){
        UserDTO userDTO = findByLogin(login);
        userDTO.setLocked(false);
        entityManager.merge(userDTO);
    }

}
