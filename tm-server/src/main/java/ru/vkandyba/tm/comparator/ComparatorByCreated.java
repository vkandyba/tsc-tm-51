package ru.vkandyba.tm.comparator;

import ru.vkandyba.tm.api.entity.IHasCreated;

import java.util.Comparator;

public class ComparatorByCreated implements Comparator<IHasCreated> {

    private static ComparatorByCreated INSTANCE = new ComparatorByCreated();

    private ComparatorByCreated() {

    }

    public static ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(IHasCreated o1, IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
