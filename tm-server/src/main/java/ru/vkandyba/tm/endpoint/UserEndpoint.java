package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.IUserEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.dto.SessionDTO;
import ru.vkandyba.tm.dto.UserDTO;

import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable UserDTO viewUserInfo(@NotNull SessionDTO sessionDTO) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        return serviceLocator.getUserDtoService().findById(sessionDTO.getUserId());
    }

    @Override
    public void updateUserProfile(@NotNull SessionDTO sessionDTO, @NotNull String firstName, @NotNull String lastName, @NotNull String midName) {
        serviceLocator.getSessionDtoService().validate(sessionDTO);
        serviceLocator.getUserDtoService().updateUser(sessionDTO.getUserId(), firstName, lastName, midName);
    }

}
